package entidad;

import entidad.Espectaculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-12-05T11:12:36")
@StaticMetamodel(Boleto.class)
public class Boleto_ { 

    public static volatile SingularAttribute<Boleto, Integer> estado;
    public static volatile SingularAttribute<Boleto, String> idBoleto;
    public static volatile SingularAttribute<Boleto, Long> costo;
    public static volatile SingularAttribute<Boleto, Integer> noAsiento;
    public static volatile SingularAttribute<Boleto, Espectaculo> idEspectaculo;

}