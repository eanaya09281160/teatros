package entidad;

import entidad.Boleto;
import entidad.Localidades;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-12-05T11:12:36")
@StaticMetamodel(Espectaculo.class)
public class Espectaculo_ { 

    public static volatile SingularAttribute<Espectaculo, Date> fechahora;
    public static volatile SingularAttribute<Espectaculo, Localidades> idSalon;
    public static volatile SingularAttribute<Espectaculo, BigDecimal> precio;
    public static volatile CollectionAttribute<Espectaculo, Boleto> boletoCollection;
    public static volatile SingularAttribute<Espectaculo, String> idEspectaculo;
    public static volatile SingularAttribute<Espectaculo, String> nombre;

}