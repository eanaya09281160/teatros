package entidad;

import entidad.Categoria;
import entidad.Espectaculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-12-05T11:12:36")
@StaticMetamodel(Localidades.class)
public class Localidades_ { 

    public static volatile SingularAttribute<Localidades, Integer> idSalon;
    public static volatile SingularAttribute<Localidades, String> lugar;
    public static volatile CollectionAttribute<Localidades, Espectaculo> espectaculoCollection;
    public static volatile SingularAttribute<Localidades, Categoria> idCategoria;
    public static volatile SingularAttribute<Localidades, String> nombre;

}