/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Diana
 */
@Entity
@Table(name = "boleto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Boleto.findAll", query = "SELECT b FROM Boleto b"),
    @NamedQuery(name = "Boleto.findByIdBoleto", query = "SELECT b FROM Boleto b WHERE b.idBoleto = :idBoleto"),
    @NamedQuery(name = "Boleto.findByNoAsiento", query = "SELECT b FROM Boleto b WHERE b.noAsiento = :noAsiento"),
    @NamedQuery(name = "Boleto.findByCosto", query = "SELECT b FROM Boleto b WHERE b.costo = :costo"),
    @NamedQuery(name = "Boleto.findByEstado", query = "SELECT b FROM Boleto b WHERE b.estado = :estado")})
public class Boleto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 52)
    @Column(name = "id_boleto")
    private String idBoleto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "no_asiento")
    private int noAsiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "costo")
    private long costo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private int estado;
    @JoinColumn(name = "id_espectaculo", referencedColumnName = "id_espectaculo")
    @ManyToOne(optional = false)
    private Espectaculo idEspectaculo;

    public Boleto() {
    }

    public Boleto(String idBoleto) {
        this.idBoleto = idBoleto;
    }

    public Boleto(String idBoleto, int noAsiento, long costo, int estado) {
        this.idBoleto = idBoleto;
        this.noAsiento = noAsiento;
        this.costo = costo;
        this.estado = estado;
    }

    public String getIdBoleto() {
        return idBoleto;
    }

    public void setIdBoleto(String idBoleto) {
        this.idBoleto = idBoleto;
    }

    public int getNoAsiento() {
        return noAsiento;
    }

    public void setNoAsiento(int noAsiento) {
        this.noAsiento = noAsiento;
    }

    public long getCosto() {
        return costo;
    }

    public void setCosto(long costo) {
        this.costo = costo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Espectaculo getIdEspectaculo() {
        return idEspectaculo;
    }

    public void setIdEspectaculo(Espectaculo idEspectaculo) {
        this.idEspectaculo = idEspectaculo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBoleto != null ? idBoleto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boleto)) {
            return false;
        }
        Boleto other = (Boleto) object;
        if ((this.idBoleto == null && other.idBoleto != null) || (this.idBoleto != null && !this.idBoleto.equals(other.idBoleto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Boleto[ idBoleto=" + idBoleto + " ]";
    }
    
}
