/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana
 */
@Entity
@Table(name = "espectaculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Espectaculo.findAll", query = "SELECT e FROM Espectaculo e"),
    @NamedQuery(name = "Espectaculo.findByIdEspectaculo", query = "SELECT e FROM Espectaculo e WHERE e.idEspectaculo = :idEspectaculo"),
    @NamedQuery(name = "Espectaculo.findByNombre", query = "SELECT e FROM Espectaculo e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Espectaculo.findByPrecio", query = "SELECT e FROM Espectaculo e WHERE e.precio = :precio"),
    @NamedQuery(name = "Espectaculo.findByFechahora", query = "SELECT e FROM Espectaculo e WHERE e.fechahora = :fechahora")})
public class Espectaculo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "id_espectaculo")
    private String idEspectaculo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private BigDecimal precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechahora")
    @Temporal(TemporalType.DATE)
    private Date fechahora;
    @JoinColumn(name = "id_salon", referencedColumnName = "id_salon")
    @ManyToOne(optional = false)
    private Localidades idSalon;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEspectaculo")
    private Collection<Boleto> boletoCollection;

    public Espectaculo() {
    }

    public Espectaculo(String idEspectaculo) {
        this.idEspectaculo = idEspectaculo;
    }

    public Espectaculo(String idEspectaculo, String nombre, BigDecimal precio, Date fechahora) {
        this.idEspectaculo = idEspectaculo;
        this.nombre = nombre;
        this.precio = precio;
        this.fechahora = fechahora;
    }

    public String getIdEspectaculo() {
        return idEspectaculo;
    }

    public void setIdEspectaculo(String idEspectaculo) {
        this.idEspectaculo = idEspectaculo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Date getFechahora() {
        return fechahora;
    }

    public void setFechahora(Date fechahora) {
        this.fechahora = fechahora;
    }

    public Localidades getIdSalon() {
        return idSalon;
    }

    public void setIdSalon(Localidades idSalon) {
        this.idSalon = idSalon;
    }

    @XmlTransient
    public Collection<Boleto> getBoletoCollection() {
        return boletoCollection;
    }

    public void setBoletoCollection(Collection<Boleto> boletoCollection) {
        this.boletoCollection = boletoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEspectaculo != null ? idEspectaculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Espectaculo)) {
            return false;
        }
        Espectaculo other = (Espectaculo) object;
        if ((this.idEspectaculo == null && other.idEspectaculo != null) || (this.idEspectaculo != null && !this.idEspectaculo.equals(other.idEspectaculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Espectaculo[ idEspectaculo=" + idEspectaculo + " ]";
    }
    
}
